import React from 'react';
import * as d3 from "d3";
import { useEffect, useState } from 'react';

export default function LinePlot({
  allFieldsData,
  height = 150,
  marginTop = 20+40,
  marginRight = 20+71,
  marginBottom = 20+30,
  marginLeft = 20
}) {
  const [width, setWidth] = useState(640);
  let x, y, line, x2, y2, line2, tideData, data;
  const tideColor = "green";
  function computeGraph() {
	  if(window.innerWidth <= 600) {
	    allFieldsData = allFieldsData.slice(6);
	  }
	  tideData = allFieldsData.map(json => json.tide);
    data = allFieldsData.map(json => json.sessionStoodUpCandidatesXGroup);
	  x = d3.scaleLinear([0, data.length - 1], [marginLeft, width - marginRight]);
	  y = d3.scaleLinear(d3.extent(data), [height - marginBottom, marginTop]);
	  line = d3.line((d, i) => x(i), y);
	  x2 = d3.scaleLinear([0, tideData.length - 1], [marginLeft, width - marginRight]);
	  y2 = d3.scaleLinear(d3.extent(tideData), [height - marginBottom, marginTop]);
	  line2 = d3.line((d, i) => x2(i), y2);
	}
	computeGraph();
	
	useEffect(() => {
	  setWidth(window.innerWidth);
		addEventListener("resize", (event) => {
		  setWidth(window.innerWidth);
		});
		screen.orientation.addEventListener("change", (event) => {
		  setWidth(window.innerWidth);
		});
	});
	
  return (
	  <>
	    <svg width="100%" height={height}>
	    	<text x={marginLeft} y="20" fill="currentColor">tall presence —</text>
	    	<text x={marginLeft} y="40" fill={tideColor}>tide --</text>
	      <path fill="none" stroke="currentColor" strokeWidth="1.5" d={line(data)} />
	      <g fill="white" stroke="currentColor" strokeWidth="1.5">
	        {data.map((d, i) => (<circle key={i} cx={x(i)} cy={y(d)} r="2.5" />))}
	      </g>
        {allFieldsData.map((d, i) => 
        	(
        		<text 
        			key={i}
        			className="tall-presence-point"
        			x={x(i)}
        			y={y(d.sessionStoodUpCandidatesXGroup)}
        			fill="currentColor">
        				<tspan 
        					x={x(i) + 15}
        					dy="1.2em">{d.sessionStoodUpCandidatesXGroup}
      					</tspan> at 
      					<tspan
      						x={x(i) + 15}
      						dy="1.2em">{new Date(d.time).toLocaleString('en-US', { month: 'numeric', day: 'numeric' })}
    						</tspan>
      					<tspan
      						x={x(i) + 15}
      						dy="1.2em">{new Date(d.time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric' })
      						.replace('AM','A')
      						.replace('PM','P')}
    						</tspan>
  						</text>
						)
					)}
	      <path fill="none" stroke="green" strokeWidth="1.5" strokeDasharray="3, 3" d={line2(tideData)} />
	      <g fill="white" stroke={tideColor} strokeWidth="1.5">
	        {tideData.map((d, i) => (<circle key={i} cx={x2(i)} cy={y2(d)} r="2.5" />))}
	      </g>
        {tideData.map((d, i) => (<text key={i} className="tide-point" x={x2(i)} y={y2(d)} fill={tideColor}>{+(d.toFixed(1))}</text>))}
	    </svg>
	  </>
  );
}