import React from 'react';
import { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from '../error-boundary';
import "bootstrap";
import { createRoot } from "react-dom/client"; ///client
import "./check-cowells.scss";
import PrivacyNotice from '../shared/privacy-notice';
import LinePlot from './linePlot.jsx';
'use strict';

export default function CheckCowellsFrontEndFunction() {
  const [tallPresence, setTallPresence] = useState(0);
  const [showPrivacyNotice, setShowPrivacyNotice] = useState(true);
  const [updateTime, setUpdateTime] = useState(0);
  const [incomingResults, setIncomingResults] = useState([]);
  const [incomingSignificants, setIncomingSignificants] = useState([]);
  const [incomingTallPresenceSignificants, setIncomingTallPresenceSignificants] = useState([]);
  const [incomingTideSignificants, setIncomingTideSignificants] = useState([]);
  const [minimumTallPresence, setMinimumTallPresence] = useState(1);
  const [isMounted, setIsMounted] = useState(false);
  const [timesRendered, setTimesRendered] = useState(0);
  const [fetchedResultsOnce, setFetchedResultsOnce] = useState(false);
  const [takingLastSignificant, setTakingLastSignificant] = useState(false);
  const [screenshotsPresented, setScreenshotsPresented] = useState(0);
  const VIEWS = [
    'outside',
    'inside',
    'wide',
  ];
  const screenshotsPresentationOutput = [];
  const getScreenshotsPresentation = (jsonArray = incomingResults) => {
    //cumulate stoodUpCandidates for each sampling, collect at every multiple of 3 index
    let alreadyAddedThead = false;
    screenshotsPresentationOutput.length = 0;
    jsonArray.sort((a,b) => a.scanNumber - b.scanNumber);
    const numberOfScans = jsonArray.length / VIEWS.length;
    for(let i=0; i < numberOfScans; i++) {
      let samplingTallPresence = 0;      
      // consolidate numbers from the zones
      samplingTallPresence = jsonArray.filter(json => json.scanNumber === i)
      .reduce((accumulator, currentValue) => accumulator + currentValue.stoodUpCandidatesXGroup, 0);
      if(samplingTallPresence >= minimumTallPresence) {
        screenshotsPresentationOutput.push(
          <tr key={jsonArray[i].time}>
            <td>
              {samplingTallPresence}
            </td>
            <td>
              <img src={"cowells-wide"+(takingLastSignificant ? "-significant" : "")+"-"+ i + ".png"}/>
            </td>
          </tr>
        );
        samplingTallPresence = 0;
        const headerRow = document.createElement('tr');
        let tableHeader = document.createElement('th');
        tableHeader.append("Tall Presence");
        headerRow.appendChild(tableHeader);
        tableHeader = document.createElement('th');
        tableHeader.append(" | Wide view");
        headerRow.appendChild(tableHeader);
        if(screenshotsPresentationOutput.length > 0 
          && document.getElementsByTagName('th').length < 1) {
          document.getElementsByTagName('thead')[0].appendChild(headerRow);
          alreadyAddedThead = true;
        } 
      }
      else if (screenshotsPresentationOutput.length < 1
        && document.getElementsByTagName('th').length > 0
        ) {
        document.getElementsByTagName('thead')[0].removeChild(document.getElementsByTagName('thead')[0].children[0]);
      }
    }

    return (<tbody>
      {screenshotsPresentationOutput}
    </tbody>);
  };
  let decreaseMinimumTallPresence = () => {
    if(isMounted) {
      let adjustedTallPresence = minimumTallPresence - 1;
      setMinimumTallPresence(adjustedTallPresence);
      localStorage.setItem('minimumTallPresence', adjustedTallPresence);
    }
  };
  let increaseMinimumTallPresence = () => {
    if(isMounted) {
      let adjustedTallPresence = minimumTallPresence + 1;
      setMinimumTallPresence(adjustedTallPresence);
      localStorage.setItem('minimumTallPresence', adjustedTallPresence);
    }
  };
  
  useEffect(() => {
    let totalStoodUpCandidates = 0;
    let totalStoodUpCandidatesXGroup = 0;
    async function fetchResults(file='cowells-out.json') {
      setFetchedResultsOnce(true);
      return (new Promise(resolve => {
        if(isMounted) {
          let maximumStoodUpCandidates = 0;
          return fetch(file)
          .then((response) => response.json())
          .then((jsonArray) => {
            setIncomingResults(jsonArray);
            jsonArray.forEach((json, i) => {
              if(i === 0) {
                setUpdateTime(json.time);
              }
              if(
                ['outside', 'inside'].indexOf(json.areaName) > -1
                ) {
                totalStoodUpCandidates += json.stoodUpCandidates;
                totalStoodUpCandidatesXGroup += json.stoodUpCandidatesXGroup;
              }
            });
            setTallPresence(totalStoodUpCandidatesXGroup);
            setScreenshotsPresented(screenshotsPresentationOutput.length);
            resolve(totalStoodUpCandidatesXGroup);
          });
        }
      }));
    }
    setTimeout(async () => {
      setIsMounted(true);
      setMinimumTallPresence(Number(localStorage.getItem('minimumTallPresence')));
      fetchResults().then(totalStoodUpCandidates => {
        if(totalStoodUpCandidates === 0) {
          setTakingLastSignificant(true)
          fetchResults('cowells-out-significant.json')
        }
      });
    }, 0);
    async function fetchRecentSignificants(file='cowells-out-significants.json') {
      return (new Promise(resolve => {
        if(isMounted) {
          let maximumStoodUpCandidates = 0;
          return fetch(file)
          .then(response => response.json())
          .then(jsonArray => {
            const chartData = jsonArray.map(json => json.sessionStoodUpCandidatesXGroup);
            setIncomingSignificants(jsonArray);
            resolve();
          });
        }
      }));
    }
    fetchRecentSignificants();
    let maximumTallPresence = 0;
    let maximumTallPresenceTime = 0;
  }, [fetchedResultsOnce]);

  return (
    <div id="checkCowellsBody" className={location.host.indexOf('3999') > -1 ? 'hide' : ''}>
      <div className={'taking-last-significant ' + (takingLastSignificant ? '' : 'hide')}>
        <p>Last check did not detect tall presence, defaulting to last significant tall presence further down. Set minimum threshold to 0 to see all.</p>
        <p><img src="cowells-wide-0.png"/></p>
      </div>
      {takingLastSignificant ? <></> : null}
      <p>Possible tall presence factor is {tallPresence}. 7 or above has been more likely. Presences are higher during low tides and higher waves.</p>
      <LinePlot 
        allFieldsData={incomingSignificants}
      />
      <div>Minimum tall presence threshold photos shown below: <button className="decrease-minimum" onClick={decreaseMinimumTallPresence}>-</button> <span className="minimum-tall-presence">{minimumTallPresence}</span> <button className="increase-minimum" onClick={increaseMinimumTallPresence}>+</button></div>
      <p>updated {new Date(updateTime).toLocaleDateString('en-us', { weekday:"long", year:"numeric", month:"short", day:"numeric", hour:"numeric", minute:"numeric"})} Pacific</p>
      <table id="results">
        <thead>
        </thead>
        {getScreenshotsPresentation()}
      </table>  
      <PrivacyNotice/>
    </div>
  );
  
}
const root = createRoot(document.getElementById('checkCowells'));
root.render(
  <ErrorBoundary>
    <CheckCowellsFrontEndFunction/>
  </ErrorBoundary> 
)